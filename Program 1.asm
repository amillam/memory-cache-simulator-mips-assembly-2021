# For part 2 of CLC - Memory Cache Simulator and MIPS Assembly
# Purpose of this program: Demonstrate progress in learning MIPS assembly by
# writing a program that inputs two numbers from the user and displays their sum.

.text
main:
	# prompt user to enter two numbers
	la $a0, prompt1
	li $v0, 4
	syscall
	li $v0, 5
	syscall
	move $t0, $v0

	la $a0, prompt2
	li $v0, 4
	syscall
	li $v0, 5
	syscall
	move $t1, $v0

	# calculate sum
	add $t2, $t0, $t1

	# display sum of numbers and exit program
	la $a0, outputDescription
	li $v0, 4
	syscall
	move $a0, $t2
	li $v0, 1
	syscall

	li $v0, 10
	syscall
	
	
.data
	prompt1: .asciiz "Type two numbers to compute their sum.\n  First number: "
	prompt2: .asciiz "  Second number: "
	outputDescription: .asciiz "\nSum: "