# For part 2 of CLC - Memory Cache Simulator and MIPS Assembly
# Purpose of this program: Demonstrate progress in learning MIPS assembly by writing
# a program that inputs two numbers from the user and displays the greater number.

.text
main:
	# prompt user to enter two numbers
	la $a0, prompt1
	li $v0, 4
	syscall
	li $v0, 5
	syscall
	move $t0, $v0

	la $a0, prompt2
	li $v0, 4
	syscall
	li $v0, 5
	syscall
	move $t1, $v0

	# find greatest number
	bgt $t1, $t0, else
		move $t2, $t0  # if above line didn't jump to "else:", $t0 is greater
		j end_if  # skip the "else:" block
	else:  # $t1 is greater
		move $t2, $t1

	# display sum of numbers and exit program
	end_if: la $a0, outputDescription
	li $v0, 4
	syscall
	move $a0, $t2
	li $v0, 1
	syscall

	li $v0, 10
	syscall
	
	
.data
	prompt1: .asciiz "Type two numbers to find the greatest value.\n  First number: "
	prompt2: .asciiz "  Second number: "
	outputDescription: .asciiz "\nGreatest number: "